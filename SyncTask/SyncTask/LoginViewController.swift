//
//  LoginViewController.swift
//  SyncTask
//
//  Created by Francisco Navarro Aguilar on 1/12/20.
//  Copyright © 2020 Fran Navarro. All rights reserved.
//

import Foundation
import TwitterKit

final class LoginViewController: UIViewController {

    private var loginButton: TWTRLogInButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        addLoginButton()
    }

    private func addLoginButton() {
        loginButton = TWTRLogInButton { [weak self] (session, error) in
            if let err = error {
                print(err.localizedDescription)
            } else {
                self?.show(MapViewController(nibName: nil, bundle: nil), sender: nil)
                UserDefaults.standard.set(session?.userID, forKey: .userIdKey)
            }
        }
        view.addSubview(loginButton)
        loginButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            loginButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            loginButton.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
    }
}
