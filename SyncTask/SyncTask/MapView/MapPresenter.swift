//
//  MapPresenter.swift
//  SyncTask
//
//  Created by Francisco Navarro Aguilar on 1/13/20.
//  Copyright © 2020 Fran Navarro. All rights reserved.
//

import Foundation

protocol MapPresentationLogic {
    func presentTweets(_ tweets: [Tweet])
    func presentError(_ error: Error)
}

final class MapPresenter: MapPresentationLogic {
    weak var viewController: MapDisplayLogic?

    func presentTweets(_ tweets: [Tweet]) {
        viewController?.displayTweets(tweets)
    }

    func presentError(_ error: Error) {
        viewController?.displayError(error)
    }

}
