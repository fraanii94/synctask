//
//  MapViewController.swift
//  SyncTask
//
//  Created by Fran Navarro on 12/01/2020.
//  Copyright © 2020 Fran Navarro. All rights reserved.
//

import UIKit
import MapKit
import SwifteriOS
import TwitterKit

protocol MapDisplayLogic: class {
    func displayTweets(_ tweets: [Tweet])
    func displayError(_ error: Error)
}

final class MapViewController: UIViewController, MapDisplayLogic {

    var stackView: UIStackView!
    var searchBar: UISearchBar!
    var mapView: CustomMapView!
    var client: String = ""
    var interactor: MapBusinessLogic?

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        addStackView()
        addSearchBar()
        addMapView()
    }

    private func setup() {
        let interactor = MapInteractor()
        let presenter = MapPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }

    private func addStackView() {
        stackView = UIStackView()
        stackView.axis = .vertical
        stackView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(stackView)
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: view.topAnchor),
            stackView.leftAnchor.constraint(equalTo: view.leftAnchor),
            stackView.rightAnchor.constraint(equalTo: view.rightAnchor),
            stackView.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
    }

    private func addSearchBar() {
        searchBar = UISearchBar()
        searchBar.placeholder = "Search tweets"
        searchBar.delegate = self
        searchBar.searchTextField.textColor = .white
        navigationController?.navigationBar.barStyle = .blackOpaque
        navigationController?.navigationBar.isTranslucent = false
        navigationItem.titleView = searchBar
    }

    private func addMapView() {
        mapView = CustomMapView()
        mapView.region = Constants.defaultRegion
        stackView.addArrangedSubview(mapView)
    }

    func displayTweets(_ tweets: [Tweet]) {
        tweets.forEach { [weak self] tweet in
            self?.mapView.addAnnotation(for: tweet, withTimeInterval: 20)
        }
    }

    func displayError(_ error: Error) {
        mapView.stopLifespan()
    }
}

extension MapViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        guard let text = searchBar.text else { return }
        interactor?.searchTweets(using: text)
    }
}
