//
//  CustomMapView.swift
//  SyncTask
//
//  Created by Francisco Navarro Aguilar on 1/12/20.
//  Copyright © 2020 Fran Navarro. All rights reserved.
//

import Foundation
import MapKit

final class CustomMapView: MKMapView {

    var timers: [Timer] = []

    func addAnnotation(for tweet: Tweet, withTimeInterval timeInterval: TimeInterval) {
        guard let geo = tweet.geo else { return }
        let annotation = MKPointAnnotation()
        annotation.title = tweet.text
        annotation.coordinate = CLLocationCoordinate2D(latitude: geo.coordinates[0] , longitude: geo.coordinates[1] )
        addAnnotation(annotation)
        let timer = createTimer(timeInterval, for: annotation)
        timers.append(timer)
    }

    @objc func timerDidFire(_ timer: Timer) {
        guard let annotation = timer.userInfo as? MKAnnotation else { return }
        removeAnnotation(annotation)
    }

    func stopLifespan() {
        timers.forEach {
            $0.invalidate()
        }
    }

    func removeAllAnnotations() {
        annotations.forEach {
            removeAnnotation($0)
        }

        timers.removeAll()
    }

    private func createTimer(_ timeInterval: TimeInterval, for annotation: MKAnnotation) -> Timer {
        return Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(timerDidFire(_:)), userInfo: annotation, repeats: false)
    }
}
