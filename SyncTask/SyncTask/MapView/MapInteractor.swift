//
//  MapInteractor.swift
//  SyncTask
//
//  Created by Francisco Navarro Aguilar on 1/13/20.
//  Copyright © 2020 Fran Navarro. All rights reserved.
//

import Foundation
import CoreData

protocol MapBusinessLogic {

    var stream: Stream { get set }

    func searchTweets(using track: String)
}

final class MapInteractor: MapBusinessLogic {

    var presenter: MapPresentationLogic?
    var stream: Stream = Stream()

    func searchTweets(using track: String) {
        stream.search(track: track) { [weak self] (tweets, error) in
            if let err = error{
                self?.presenter?.presentError(err)
            } else {
                print(tweets)
                let filteredTweets = tweets.filter { $0.geo != nil }
                self?.presenter?.presentTweets(filteredTweets)

            }
        }
    }



}
