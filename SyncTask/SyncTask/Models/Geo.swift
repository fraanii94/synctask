//
//  Geo.swift
//  SyncTask
//
//  Created by Francisco Navarro Aguilar on 1/13/20.
//  Copyright © 2020 Fran Navarro. All rights reserved.
//

import Foundation

struct Geo: Codable {
    let coordinates: Array<Double>
}
