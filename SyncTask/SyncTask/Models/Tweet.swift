//
//  Tweet.swift
//  SyncTask
//
//  Created by Francisco Navarro Aguilar on 1/13/20.
//  Copyright © 2020 Fran Navarro. All rights reserved.
//

import Foundation

struct Tweet: Codable {

    let text: String
    let geo: Geo?

}
