//
//  UserDefaults.swift
//  SyncTask
//
//  Created by Francisco Navarro Aguilar on 1/12/20.
//  Copyright © 2020 Fran Navarro. All rights reserved.
//

import Foundation

enum UserDefaultsKeys: String {
    case userIdKey = "TWITTER_USER_ID_KEY"
}

extension UserDefaults {
    static var userId: String? {
        get { UserDefaults.standard.value(forKey: .userIdKey) as? String }
        set { UserDefaults.standard.set(newValue, forKey: .userIdKey) }
    }

    func value(forKey key: UserDefaultsKeys) -> Any? {
        return UserDefaults.standard.value(forKey: key.rawValue)
    }

    func set(_ value: Any?, forKey key: UserDefaultsKeys) {
        UserDefaults.standard.set(value, forKey: key.rawValue)
    }

}
