//
//  Stream.swift
//  SyncTask
//
//  Created by Francisco Navarro Aguilar on 1/12/20.
//  Copyright © 2020 Fran Navarro. All rights reserved.
//

import Foundation
import TwitterKit

enum NetworkError: Error {
    case streamError
}

final class Stream: NSObject {
    
    fileprivate var urlSession: URLSession?
    fileprivate var completionHandler: (([Tweet], Error?) -> Void)?
    fileprivate var client: TWTRAPIClient? {
        guard let userId = UserDefaults.standard.value(forKey: .userIdKey) as? String else {
            return nil
        }
        return TWTRAPIClient(userID: userId)
    }


    func search(track: String, completionHandler: (([Tweet], Error?) -> Void)?) {
        self.completionHandler = completionHandler
        let task = createConnection(with: track)

        task?.resume()
    }

    func stopSearch() {
        urlSession?.invalidateAndCancel()
    }

    private func createConnection(with track: String) -> URLSessionTask? {
        urlSession = URLSession(configuration: .default, delegate: self, delegateQueue: nil)
        var error: NSError?
        guard var request = client?.urlRequest(withMethod: "POST", urlString: "https://stream.twitter.com/1.1/statuses/filter.json?track=\(track)", parameters: nil, error: &error) else { return nil }
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        return urlSession?.dataTask(with: request)
    }
}

extension Stream: URLSessionDataDelegate {
    func urlSession(_ session: URLSession, task: URLSessionTask, didReceive challenge: URLAuthenticationChallenge,
                    completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {

        let sameHost = task.originalRequest?.url?.host == challenge.protectionSpace.host
        let isAuthMethod = challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust

        guard let serverTrust = challenge.protectionSpace.serverTrust,
            sameHost && isAuthMethod else {
                completionHandler(.cancelAuthenticationChallenge, nil)
                return
        }

        completionHandler(.useCredential, URLCredential(trust: serverTrust))
    }

    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        let stringData = String(data: data, encoding: .utf8) ?? ""
        let tweets = stringData
            .components(separatedBy: "\r\n")
            .compactMap { $0.data(using: .utf8) }
            .compactMap { try? JSONDecoder().decode(Tweet.self, from: $0) }
        
        DispatchQueue.main.async { [weak self] in
            print(tweets)
            self?.completionHandler?(tweets, nil)
        }

    }

    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        completionHandler?([], error)
    }


}
